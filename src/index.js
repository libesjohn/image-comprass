import { Matrix, SingularValueDecomposition } from 'ml-matrix';
let canvas = document.getElementById('viewport'),
    context = canvas.getContext('2d');
let canvas2 = document.getElementById('viewport2'),
    context2 = canvas2.getContext('2d');
let slider = document.getElementById('myRange');
let loader = document.getElementById('loader-con');
let downloadButton = document.getElementById('download');
let value = document.getElementById('value');
const base_image = new Image();
let width = 512;
let reduceBy = 10// 0-100;
// slider.setAttribute('value', reduceBy);
let reduced_matrix_size = Math.round(width * (reduceBy / 100));

slider.addEventListener("change", (event) => {
    loader.classList.toggle('show');
    setTimeout(() => {
        reduceBy = event.target.value;
        value.innerText = reduceBy;
        reduced_matrix_size = Math.round(width * (reduceBy / 100));
        changeImageQuality();
        loader.classList.toggle('show');
    }, 0)
})

downloadButton.addEventListener("click", (event) => {
    var link = document.createElement('a');
    link.download = 'compressed-imgae.png';
    link.href = canvas2.toDataURL();
    link.click();
})

canvas.style.visibility = "hidden";
canvas2.style.visibility = 'hidden';
const changeImageQuality = () => {
    const matrix = extractImageMatrix(); // convert image to pixel matrix [[,],[,]] multi-d-array;
    let { U, V, diagonalMatrix } = new SingularValueDecomposition(new Matrix(matrix)); // Calc the svd params;
    V = V.transpose();
    let reduced_U = U.subMatrix(0, width - 1, 0, reduced_matrix_size); // comprass the image by reducing the matrices sizes by new_i;
    let reduced_V = V.subMatrix(0, reduced_matrix_size, 0, width - 1);
    let reduced_D = diagonalMatrix.subMatrix(0, reduced_matrix_size, 0, reduced_matrix_size);
    const comprasseddMatrix = multiply3Matrices(reduced_U, reduced_D, reduced_V); // mutyply the u,v,d to get the comprassed image;
    drawComprasseddMatrixImage(comprasseddMatrix); // convert the new matrix to image and draw it;
    canvas.style.visibility = 'visible';
    canvas2.style.visibility = 'visible';
}
base_image.onload = () => {
    loader.classList.toggle('show');
    setTimeout(()=>{
        value.innerText = reduceBy;
        changeImageQuality();
        loader.classList.toggle('show');
    },0)
}
base_image.src = './image.png';
function multiply3Matrices(m1, m2, m3) {
    return multiplyMatrices(multiplyMatrices(m1.data, m2.data), m3.data);
}
function multiplyMatrices(m1, m2) {
    var result = [];
    for (var i = 0; i < m1.length; i++) {
        result[i] = [];
        for (var j = 0; j < m2[0].length; j++) {
            var sum = 0;
            for (var k = 0; k < m1[0].length; k++) {
                sum += m1[i][k] * m2[k][j];
            }
            result[i][j] = sum;
        }
    }
    return result;
}
function extractImageMatrix() {
    const matrix = [];
    context.drawImage(base_image, 0, 0);
    var data = context.getImageData(0, 0, width, width);
    for (let i = 0; i < width; i++) {
        let row = [];
        for (let j = 0; j < width; j++) {
            const pixel = data.data[(i * width + j) * 4];
            row.push(pixel);
        }
        matrix.push(row);
    }
    return matrix;
}
function drawComprasseddMatrixImage(comprasseddMatrix) {
    // first, create a new ImageData to contain our pixels
    const imgData = context2.createImageData(width, width); // width x height
    const data = imgData.data;
    // copy img byte-per-byte into our ImageData
    let k = 0;
    for (let i = 0; i < width; i++) {
        for (let j = 0; j < width; j++) {
            data[k] = comprasseddMatrix[i][j]; // R
            data[k + 1] = comprasseddMatrix[i][j]; // G
            data[k + 2] = comprasseddMatrix[i][j]; // B
            data[k + 3] = 255; // A
            k += 4;
        }
    }
    // now we can draw our imagedata onto the canvas
    context2.putImageData(imgData, 0, 0);
}